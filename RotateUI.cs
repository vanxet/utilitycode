using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RotateUI : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    public Camera cam;
    public Transform[] childItems;

    private Quaternion originalRotation;
    private float startAngle;
    private Coroutine smoothCoroutine;
    private float angleUnit = 30;

    public void Start()
    {
        originalRotation = transform.rotation;
    }

    void RotateChildItem()
    {
        for (int i = 0; i < childItems.Length; i++)
        {
            childItems[i].rotation = Quaternion.identity;
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (smoothCoroutine != null)
            StopCoroutine(smoothCoroutine);

        originalRotation = transform.rotation;
        Vector3 screenPos = RectTransformUtility.WorldToScreenPoint(cam, transform.position);

#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
        Vector3 vector = (Vector3) Input.GetTouch(0).position - screenPos;
#else
        Vector3 vector = Input.mousePosition - screenPos;
#endif

        startAngle = Mathf.Atan2(vector.y, vector.x) * Mathf.Rad2Deg;
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector3 screenPos = RectTransformUtility.WorldToScreenPoint(cam, transform.position);

#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
        Vector3 vector = (Vector3) Input.GetTouch(0).position - screenPos;
#else
        Vector3 vector = Input.mousePosition - screenPos;
#endif

        float angle = Mathf.Atan2(vector.y, vector.x) * Mathf.Rad2Deg;
        Quaternion newRotation = Quaternion.AngleAxis(angle - startAngle, transform.forward);
        newRotation.y = 0;
        newRotation.eulerAngles = new Vector3(0, 0, newRotation.eulerAngles.z);
        transform.rotation = originalRotation * newRotation;

        RotateChildItem();
    }


    public void OnEndDrag(PointerEventData eventData)
    {
        float z = transform.rotation.eulerAngles.z;
        int index = Mathf.FloorToInt(z / angleUnit);
        float sub = z - index * angleUnit;
        float end = sub > angleUnit / 2 ? index + 1 : index;

        smoothCoroutine = StartCoroutine(SmoothMove(end * angleUnit));
    }

    IEnumerator SmoothMove(float angleEnd)
    {
        float angleStart = transform.rotation.eulerAngles.z;
        float distance = angleEnd - angleStart;
        float unit = distance * Time.deltaTime * (1 / 0.15f);
        if (distance != 0)
        {
            while (!Mathf.Approximately(angleStart, angleEnd))
            {
                angleStart += unit;
                if (distance > 0)
                {
                    angleStart = angleStart > angleEnd ? angleEnd : angleStart;
                }
                else
                {
                    angleStart = angleStart < angleEnd ? angleEnd : angleStart;
                }

                transform.rotation = Quaternion.Euler(new Vector3(0, 0, angleStart));
                RotateChildItem();

                yield return null;
            }
        }

        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angleEnd));
        RotateChildItem();
    }
}